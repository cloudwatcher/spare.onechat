//The Jquery Blinking Plugin
(function($)
{
		$.fn.blink = function(options)
		{
				var defaults = { delay:500 };
				var options = $.extend(defaults, options);
				
				return this.each(function()
				{
						var obj = $(this);
						if (obj.attr("timerid") > 0) return;
						var timerid=setInterval(function()
						{
				if (($(obj).css("color") == "rgb(255, 255, 255)") || ($(obj).css("color") == "rgb(255,255,255)"))
				{
					$(obj).animate({color: '#999'}, Math.round(options.delay/2));
				}
				else
				{
					$(obj).animate({color: '#fff'}, Math.round(options.delay/2));
				};
				}, options.delay);
						obj.attr("timerid", timerid);
				});
		}
		$.fn.unblink = function(options) 
		{
				var defaults = { visible:true };
				var options = $.extend(defaults, options);
				
				return this.each(function() 
				{
						var obj = $(this);
						if (obj.attr("timerid") > 0) 
						{
								clearInterval(obj.attr("timerid"));
								obj.attr("timerid", 0);
								obj.css('visibility', options.visible?'visible':'hidden');
						}
				});
		}
}(jQuery))

/* Script by: www.jtricks.com
 * Version: 1.8 (20111103)
 * Latest version: www.jtricks.com/javascript/navigation/floating.html
 *
 * License:
 * GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 */
var floatingMenu =
{
	hasInner: typeof(window.innerWidth) == 'number',
	hasElement: typeof(document.documentElement) == 'object'
		&& typeof(document.documentElement.clientWidth) == 'number'
};

var floatingArray =
[
];

floatingMenu.add = function(obj, options)
{
	var name;
	var menu;

	if (typeof(obj) === "string")
		name = obj;
	else
		menu = obj;
		

	if (options == undefined)
	{
		floatingArray.push( 
			{
				id: name,
				menu: menu,

				targetLeft: 0,
				targetTop: 0,

				distance: .07,
				snap: true
			});
	}
	else
	{
		floatingArray.push( 
			{
				id: name,
				menu: menu,

				targetLeft: options.targetLeft,
				targetRight: options.targetRight,
				targetTop: options.targetTop,
				targetBottom: options.targetBottom,

				centerX: options.centerX,
				centerY: options.centerY,

				prohibitXMovement: options.prohibitXMovement,
				prohibitYMovement: options.prohibitYMovement,

				distance: options.distance != undefined ? options.distance : .07,
				snap: options.snap,
				ignoreParentDimensions: options.ignoreParentDimensions,

				scrollContainer: options.scrollContainer,
				scrollContainerId: options.scrollContainerId
			});
	}
};

floatingMenu.findSingle = function(item)
{
	if (item.id)
		item.menu = document.getElementById(item.id);

	if (item.scrollContainerId)
		item.scrollContainer = document.getElementById(item.scrollContainerId);
};

floatingMenu.move = function (item)
{
	if (!item.prohibitXMovement)
	{
		item.menu.style.left = item.nextX + 'px';
		item.menu.style.right = '';
	}

	if (!item.prohibitYMovement)
	{
		item.menu.style.top = item.nextY + 'px';
		item.menu.style.bottom = '';
	}
};

floatingMenu.scrollLeft = function(item)
{
	// If floating within scrollable container use it's scrollLeft
	if (item.scrollContainer)
		return item.scrollContainer.scrollLeft;

	var w = window.top;

	return this.hasInner
		? w.pageXOffset  
		: this.hasElement  
		  ? w.document.documentElement.scrollLeft  
		  : w.document.body.scrollLeft;
};

floatingMenu.scrollTop = function(item)
{
	// If floating within scrollable container use it's scrollTop
	if (item.scrollContainer)
		return item.scrollContainer.scrollTop;

	var w = window.top;

	return this.hasInner
		? w.pageYOffset
		: this.hasElement
		  ? w.document.documentElement.scrollTop
		  : w.document.body.scrollTop;
};

floatingMenu.windowWidth = function()
{
	return this.hasElement
		? document.documentElement.clientWidth
		: document.body.clientWidth;
};

floatingMenu.windowHeight = function()
{
	if (floatingMenu.hasElement && floatingMenu.hasInner)
	{
		// Handle Opera 8 problems
		return document.documentElement.clientHeight > window.innerHeight
			? window.innerHeight
			: document.documentElement.clientHeight
	}
	else
	{
		return floatingMenu.hasElement
			? document.documentElement.clientHeight
			: document.body.clientHeight;
	}
};

floatingMenu.documentHeight = function()
{
	var innerHeight = this.hasInner
		? window.innerHeight
		: 0;

	var body = document.body,
		html = document.documentElement;

	return Math.max(
		body.scrollHeight,
		body.offsetHeight, 
		html.clientHeight,
		html.scrollHeight,
		html.offsetHeight,
		innerHeight);
};

floatingMenu.documentWidth = function()
{
	var innerWidth = this.hasInner
		? window.innerWidth
		: 0;

	var body = document.body,
		html = document.documentElement;

	return Math.max(
		body.scrollWidth,
		body.offsetWidth, 
		html.clientWidth,
		html.scrollWidth,
		html.offsetWidth,
		innerWidth);
};

floatingMenu.calculateCornerX = function(item)
{
	var offsetWidth = item.menu.offsetWidth;

	if (item.centerX)
		return this.scrollLeft(item) + (this.windowWidth() - offsetWidth)/2;

	var result = this.scrollLeft(item) - item.parentLeft;
	if (item.targetLeft == undefined)
	{
		result += this.windowWidth() - item.targetRight - offsetWidth;
	}
	else
	{
		result += item.targetLeft;
	}
		
	if (document.body != item.menu.parentNode
		&& result + offsetWidth >= item.confinedWidthReserve)
	{
		result = item.confinedWidthReserve - offsetWidth;
	}

	if (result < 0)
		result = 0;

	return result;
};

floatingMenu.calculateCornerY = function(item)
{
	var offsetHeight = item.menu.offsetHeight;

	if (item.centerY)
		return this.scrollTop(item) + (this.windowHeight() - offsetHeight)/2;

	var result = this.scrollTop(item) - item.parentTop;
	if (item.targetTop === undefined)
	{
		result += this.windowHeight() - item.targetBottom - offsetHeight;
	}
	else
	{
		result += item.targetTop;
	}

	if (document.body != item.menu.parentNode
		&& result + offsetHeight >= item.confinedHeightReserve)
	{
		result = item.confinedHeightReserve - offsetHeight;
	}

	if (result < 0)
		result = 0;
		
	return result;
};

floatingMenu.computeParent = function(item)
{
	if (item.ignoreParentDimensions)
	{
		item.confinedHeightReserve = this.documentHeight();
		item.confinedWidthReserver = this.documentWidth();
		item.parentLeft = 0;  
		item.parentTop = 0;  
		return;
	}

	var parentNode = item.menu.parentNode;
	var parentOffsets = this.offsets(parentNode, item);
	item.parentLeft = parentOffsets.left;
	item.parentTop = parentOffsets.top;

	item.confinedWidthReserve = parentNode.clientWidth;

	// We could have absolutely-positioned DIV wrapped
	// inside relatively-positioned. Then parent might not
	// have any height. Try to find parent that has
	// and try to find whats left of its height for us.
	var obj = parentNode;
	var objOffsets = this.offsets(obj, item);
	while (obj.clientHeight + objOffsets.top
		   < item.menu.offsetHeight + parentOffsets.top)
	{
		obj = obj.parentNode;
		objOffsets = this.offsets(obj, item);
	}

	item.confinedHeightReserve = obj.clientHeight
		- (parentOffsets.top - objOffsets.top);
};
function logout(){$.cookie('username', null);$.cookie('password', null);window.location.replace("/")};
floatingMenu.offsets = function(obj, item)
{
	var result =
	{
		left: 0,
		top: 0
	};

	if (obj === item.scrollContainer)
		return;

	while (obj.offsetParent && obj.offsetParent != item.scrollContainer)
	{  
		result.left += obj.offsetLeft;  
		result.top += obj.offsetTop;  
		obj = obj.offsetParent;
	}  

	if (window == window.top)
		return result;

	// we're IFRAMEd
	var iframes = window.top.document.body.getElementsByTagName("IFRAME");
	for (var i = 0; i < iframes.length; i++)
	{
		if (iframes[i].contentWindow != window)
		   continue;

		obj = iframes[i];
		while (obj.offsetParent)  
		{  
			result.left += obj.offsetLeft;  
			result.top += obj.offsetTop;  
			obj = obj.offsetParent;
		}  
	}

	return result;
};

floatingMenu.doFloatSingle = function(item)
{
	this.findSingle(item);

	var stepX, stepY;

	this.computeParent(item);

	var cornerX = this.calculateCornerX(item);

	var stepX = (cornerX - item.nextX) * item.distance;
	if (Math.abs(stepX) < .5 && item.snap
		|| Math.abs(cornerX - item.nextX) == 1)
	{
		stepX = cornerX - item.nextX;
	}

	var cornerY = this.calculateCornerY(item);

	var stepY = (cornerY - item.nextY) * item.distance;
	if (Math.abs(stepY) < .5 && item.snap
		|| Math.abs(cornerY - item.nextY) == 1)
	{
		stepY = cornerY - item.nextY;
	}

	if (Math.abs(stepX) > 0 ||
		Math.abs(stepY) > 0)
	{
		item.nextX += stepX;
		item.nextY += stepY;
		this.move(item);
	}
};

floatingMenu.fixTargets = function()
{
};

floatingMenu.fixTarget = function(item)
{
};

floatingMenu.doFloat = function()
{
	this.fixTargets();
	for (var i=0; i < floatingArray.length; i++)
	{
		this.fixTarget(floatingArray[i]);
		this.doFloatSingle(floatingArray[i]);
	}
	setTimeout('floatingMenu.doFloat()', 20);
};

floatingMenu.insertEvent = function(element, event, handler)
{
	// W3C
	if (element.addEventListener != undefined)
	{
		element.addEventListener(event, handler, false);
		return;
	}

	var listener = 'on' + event;

	// MS
	if (element.attachEvent != undefined)
	{
		element.attachEvent(listener, handler);
		return;
	}

	// Fallback
	var oldHandler = element[listener];
	element[listener] = function (e)
		{
			e = (e) ? e : window.event;
			var result = handler(e);
			return (oldHandler != undefined) 
				&& (oldHandler(e) == true)
				&& (result == true);
		};
};

floatingMenu.init = function()
{
	floatingMenu.fixTargets();

	for (var i=0; i < floatingArray.length; i++)
	{
		floatingMenu.initSingleMenu(floatingArray[i]);
	}

	setTimeout('floatingMenu.doFloat()', 100);
};

// Some browsers init scrollbars only after
// full document load.
floatingMenu.initSingleMenu = function(item)
{
	this.findSingle(item);
	this.computeParent(item);
	this.fixTarget(item);
	item.nextX = this.calculateCornerX(item);
	item.nextY = this.calculateCornerY(item);
	this.move(item);
};

floatingMenu.insertEvent(window, 'load', floatingMenu.init);


// Register ourselves as jQuery plugin if jQuery is present
if (typeof(jQuery) !== 'undefined')
{
	(function ($)
	{
		$.fn.addFloating = function(options)
		{
			return this.each(function()
			{
				floatingMenu.add(this, options);
			});
		};
	}) (jQuery);
}
//The floating plugin ends here.
//Make the Interface
var appendent = "<div id='floatdiv' style='position:absolute;width:250px;top:10px;right:10px;background:rgb(255, 173, 0);border:2px solid rgb(255, 173, 0);color:white;z-index:100;font-size:15px;text-align:center;'><a href='javascript:ToggleChat();' style='text-decoration:none;color:white;' id='chattitle'><div style='padding:10px'> 在线<span id='online'>n/a</span>人 | 延迟 <span id='lag'>n/a</span> ms </div> </a><div id='input-box' style='background-color: white;color: black;text-align: left;display:none;'> <div id='chatcontents' style='height:250px;'> <ul style='overflow-y:scroll;overflow-x:hidden;height:250px;list-style-type:none;margin:0px;padding-left:0px;' id='chat-box-container'> <li style='padding-top:10px;'><div style='border-left:5px solid orange;'>客服:</div>欢迎联系客服。</li> </ul> </div> <form id='chatform'> <input type='text' id='chatinput' style='border:0px;width:100%;border-top:solid orange 1px;height:30px;'/> </form> </div> </div>";
$("body").append(appendent);
floatingMenu.add('floatdiv',  
	{  
		// Represents distance from left or right browser window  
		// border depending upon property used. Only one should be  
		// specified.  
		// targetLeft: 0,  
		targetRight: 10,  

		// Represents distance from top or bottom browser window  
		// border depending upon property used. Only one should be  
		// specified.  
		//targetTop: 10,  
		//targetBottom: 0,  

		// Uncomment one of those if you need centering on  
		// X- or Y- axis.  
		// centerX: true,  
		centerY: true,  

		// Remove this one if you don't want snap effect  
		snap: true  
	});
//This shall be the initiation code
floatingMenu.init();
//The socket connection starts here
var isopen = 0;
var uid = "";
var host = "one-auction.com";
var port_static = "8001"
var port_io = "8002"
//Open and Close
function ToggleChat()
{
	var x = $('#input-box').css('display');
	if (x=='none') {
		$('#input-box').css('display','block');
		isopen = 1;
	}else{
		$('#input-box').css('display','none');
		isopen = 0;
	};
}
$(function() {
	WEB_SOCKET_SWF_LOCATION = 'http://' + host + ':' + port_static + '/static/WebSocketMain.swf';
	var s = new io.connect('http://' + host + ':' + port_io + '/chat', {
		rememberTransport: false
		});
	var ping = new io.connect('http://' + host + ':' + port_io + '/ping');
	
	// Establish event handlers
	s.on('disconnect', function() {
		s.socket.reconnect();
	});
	
	function getPrintableDate(date) {
	  return date.getFullYear().toString() + '/' +
		 (date.getMonth()+1).toString() + '/' +
		 date.getDate().toString() + ' ' +
		 date.getHours().toString() + ':' +
		 date.getMinutes().toString() + ':' +
		 date.getSeconds().toString() + '.' +
		 date.getMilliseconds().toString();
	}
	function encodeDate(date)
	{
		return [date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()];
	}

	function decodeDate(data)
	{
		var date = new Date();
		return new Date(date.getFullYear(), date.getMonth(), date.getDate(),
						data[0], data[1], data[2], data[3]);
	}
	//now time
	function pubDate(){
		var date = new Date()
		var h = date.getHours(); 
		var m = date.getMinutes(); 
		var se = date.getSeconds(); 
		return (h<10 ? "0"+ h : h) +":" +(m<10 ? "0" + m : m) +":" +(se<10 ? "0" +se : se);
	}


	// Ping
	ping.on('message', function(data) {
		var client = decodeDate(data.client);
		var server = decodeDate(data.server);
		var total = data.total;
		var admin = data.admin;
		var now = new Date();

		$('#online').text(total);
		$('#lag').text((now.getTime() - client.getTime()).toString());            

	});

	function sendPing()
	{
		ping.json.send({client: encodeDate(new Date()) });
		setTimeout(sendPing, 5000);
	}

	sendPing();

	s.on('connect', function() {
		void(0);
	});
	  
	s.on('message', function(data) {
		var now = pubDate();
		//now should be appended
		if (data.sys != undefined) {
							$("#chat-box-container").prepend("<li style='padding-top:10px;'><div style='border-left:5px solid orange;'>系统消息" + " " + now + ":</div>"+data.sys+'</li>');
		}else if( data.chat != undefined){
							if (isopen == 0){
								$('.btn-chat a').text("你有新消息");
								$('.btn-chat a').blink();
							};
							//there should be a now appended
							var color = "orange";
							var myNick = "我";
							if (data.nick == "Service") { 
								color = "#A3FF00";
								myNick = "客服";
							};
							$("#chat-box-container").prepend("<li style='padding-top:10px;'><div style='border-left:5px solid " + color + ";'>"+ myNick + " " + now + ':</div>'+ data.chat+ "</li>");
		}else if (data.uid != undefined ){
							uid = data.uid;
							s.send('{"type":"sys","content":"' + uid + '","referrer":"'+ document.referrer +'","location":"' + document.location.href + '"}');
		}else if (data.broadcast != undefined ){
							$("#div2").text(data.broadcast);
							$("#div3").text(data.broadcast);
							$('#div1').width($('#div2').width());
							s2=getid("div2");
							s3=getid("div3");
							s4=getid("scroll");
							s4.style.width=(s2.offsetWidth*2+40)+"px";
							s3.innerHTML=s2.innerHTML;
					};
	});

	//send the message when submit is clicked
	$('#chatform').submit(function (evt) {
		var line = $('#chatinput').val();
					if(line ==''){
					   return false;
					}
		$('#chatinput').val('');
		s.send('{"type":"' + uid + '","content":"' + line + '"}');
		return false;
	});
});
