
//plotting pie chart
function PlotPie(data,place)
{
	// DEFAULT
	$.plot($(place), data,
	{
			series: {
				pie: { 
					innerRadius: 0.5,
					show: true
				}
			}
	});
}

function PlotOverView(data,place)
{
	$.plot($(place), [data],{ 
		series: { 
			lines: { show: true, lineWidth: 1 }, 
			shadowSize: 0,
			colors: [ "#000" ]
		},
		grid: { 
			borderWidth: 0,
			color: "#FFF" 
		},
		xaxis: { 
			ticks: [], mode: "time" 
		}, 
		yaxis: { 
			ticks: [], autoscaleMargin: 0.1 
		},
		selection: { mode: "x" } 
	});
}
//get the browser type
function GetBrowser()
{
	var Sys = {}; 
	var ua = navigator.userAgent.toLowerCase(); 
	var s; 
	(s = ua.match(/msie ([\d.]+)/)) ? Sys.ie = s[1] : 
	(s = ua.match(/firefox\/([\d.]+)/)) ? Sys.firefox = s[1] : 
	(s = ua.match(/chrome\/([\d.]+)/)) ? Sys.chrome = s[1] : 
	(s = ua.match(/opera.([\d.]+)/)) ? Sys.opera = s[1] : 
	(s = ua.match(/version\/([\d.]+).*safari/)) ? Sys.safari = s[1] : 0; 
	//以下进行测试 
	if (Sys.ie) return ('IE: ' + Sys.ie); 
	if (Sys.firefox) return ('Firefox: ' + Sys.firefox); 
	if (Sys.chrome) return ('Chrome: ' + Sys.chrome); 
	if (Sys.opera) return ('Opera: ' + Sys.opera); 
	if (Sys.safari) return ('Safari: ' + Sys.safari); 
}
//now time
function pubDate()
{
	var date = new Date()
	var h = date.getHours(); 
	var m = date.getMinutes(); 
	var se = date.getSeconds(); 
	return (h<10 ? "0"+ h : h) +":" +(m<10 ? "0" + m : m) +":" +(se<10 ? "0" +se : se);
}

function getPrintableDate(date) {
  return date.getFullYear().toString() + '/' +
	 (date.getMonth()+1).toString() + '/' +
	 date.getDate().toString() + ' ' +
	 date.getHours().toString() + ':' +
	 date.getMinutes().toString() + ':' +
	 date.getSeconds().toString() + '.' +
	 date.getMilliseconds().toString();
}

function encodeDate(date)
{
	return [date.getHours(), date.getMinutes(), date.getSeconds(), date.getMilliseconds()];
}

function decodeDate(data)
{
	var date = new Date();
	return new Date(date.getFullYear(), date.getMonth(), date.getDate(),
					data[0], data[1], data[2], data[3]);
}

///Other Functions
function toggleCast()
{	
	//$('#casting').button('toggle');
	if (cast == 0) {
		cast = 1;
	}else{
		cast = 0
	};
}
//Input Analyze
function analyze(referrer)
{
	var r = "";
	var reCat1 = /one-auction.com/i;
	
	if (referrer == "") {
		r = "直接着陆"
	}else{
		var result = referrer.search(reCat1);	
		switch (result)
		{
			case 7:
				r = "站内";
				break;
			default:
				r = referrer;
		}
	}; 
	return r
}
//set the target for the chat
function set_target(target)
{
	$('#chatform [type=text]').val(target+'#')
}
//Argument
function format(string)
{
  	var args = arguments;
  	var pattern = new RegExp("%([1-" + arguments.length + "])", "g");
  	return String(string).replace(pattern, function(match, index) {
    return args[index];
  	});
};
//Notify shit
function alert_new_user(holder,data)
{
	var access_source = analyze(data.referrer);
	if (access_source == "站内") { return };
	var msg_button = "<button class='btn btn-info btn-mini' onclick=javascript:set_target('%5');>向TA发送消息</button>";
	var appstr = "<blockquote class='span8 pull-right chatitem'><p>%1&nbsp<strong>系统消息</strong></p><p>%2 刚刚登陆</p><p><span class='label label-important'>%3</span></p><p><span class='label label-info'>%4</span></p><p>" + msg_button + "</p></blockquote>";
	var prependent = format(appstr,pubDate(),data.nick,data.location,access_source,data.nick);
	$(holder).prepend(prependent);		
}
//Service Chat Message
function service_chat_msg(holder,data)
{
	var appstr = "<blockquote class='span8 pull-right chatitem'><p>%1&nbsp<strong>系统消息</strong></p><p>%2</p><p><span class='label label-warning'>系统消息</span></p></blockquote>";
	var prependent = format(appstr, pubDate(), data.sys);
	$(holder).prepend(prependent);
}
//Service Message
function service_msg(holder,data)
{
	var access_source = analyze(data.referrer);
	switch (access_source) 
	{
		case "站内":
			//ignore
		default:
			//when there is a referrer
	}
	var appstr = "<li id=%1><a href=javascript:set_target('%2');><p>%3</p><p><span class='label'>%4</span></p><p><span class='label label-info'>%5</span></p><p><span class='label label-warning'>%6</span></p></a></li>";
	var prependent = format(appstr,data.nick,data.nick,data.nick, access_source, data.location,pubDate());
	$(holder).prepend(prependent);
}

//Chat Message
function chat_msg(holder,data)
{
	var pullright = 'pull-right';
	var msg_button = "<button class='btn btn-info btn-mini' onclick=javascript:set_target('%6');>向TA发送消息</button>";
	switch (data.nick)
	{
		case "Service":
			pullright = "";
			//see this solution ?
			msg_button = "";
			break;
		case "":
			data.nick = "系统消息";
			//same implementation here too
			msg_button = "";
			break;
		default:
			void(0);
	}
	var appstr = "<blockquote class='span8 %1 chatitem'><p>%2&nbsp<strong>%3</strong></p><p>%4</p><p><span class='label label-warning'>%5</span>&nbsp" + msg_button + "</p></blockquote>";
	var prependent = format(appstr,pullright,pubDate(),data.nick,data.chat,data.nick,data.nick);
	$(holder).prepend(prependent);
}